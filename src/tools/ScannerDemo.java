package tools;

import java.util.Scanner;

public class ScannerDemo {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter your firstname: \n");
        String firstName = input.nextLine();
        System.out.println("Enter your lastname: \n");
        String lastName = input.nextLine();
        System.out.println("Hello, my name is " + firstName + " " + lastName);
    }
}
